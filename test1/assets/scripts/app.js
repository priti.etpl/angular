var myApp = angular.module("myApp", ['ui.router']);
var base_url = "http://" + window.location.hostname + "/angular/test1/";
myApp.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.when("", "/login");
    $urlRouterProvider.otherwise("/login");
    $stateProvider.state("/home", {
                url: "/home",
                views: {
                    'main_bodys': {
                        templateUrl: base_url+"main_home.html"
                    }
                }    
            }).state("/home.dashboard", {
                url: "/dashboard",
                views: {
                    'menubar': {
                        templateUrl: base_url+"menubar.html"
                    },
                    'sidebar': {
                        templateUrl: base_url+"sidebar.html"
                    },
                    'bodys': {
                        templateUrl: base_url+"dashboard.html",
                        controller:"Dashboardctrl"
                    }
                }    
            }).state("/home.listing", {
                url: "/listing",
                views: {
                    'menubar': {
                        templateUrl: base_url+"menubar.html"
                    },
                    'sidebar': {
                        templateUrl: base_url+"sidebar.html"
                    },
                    'bodys': {
                        templateUrl: base_url+"listing.html",
                        controller:"Listingctrl"
                    }
                }    
            }).state("/home.gallery", {
                url: "/gallery",
                views: {
                    'menubar': {
                        templateUrl: base_url+"menubar.html"
                    },
                    'sidebar': {
                        templateUrl: base_url+"sidebar.html"
                    },
                    'bodys': {
                        templateUrl: base_url+"gallery.html",
                        controller:"Galleryctrl"
                    }
                }    
            }).state("/home.gallery.photo", {
                url: "/photo",
                views: {
                    'menubar': {
                        templateUrl: base_url+"menubar.html"
                    },
                    'sidebar': {
                        templateUrl: base_url+"sidebar.html"
                    },
                    'gallery_body': {
                        templateUrl: base_url+"photo-gallery.html",
                        controller:"Photogalleryctrl"
                    }
                }    
            }).state("/home.gallery.video", {
                url: "/video",
                views: {
                    'menubar': {
                        templateUrl: base_url+"menubar.html"
                    },
                    'sidebar': {
                        templateUrl: base_url+"sidebar.html"
                    },
                    'gallery_body': {
                        templateUrl: base_url+"video-gallery.html",
                        controller:"Videogalleryctrl"
                    }
                }    
            }).state("/home.gallery.music", {
                url: "/music",
                views: {
                    'menubar': {
                        templateUrl: base_url+"menubar.html"
                    },
                    'sidebar': {
                        templateUrl: base_url+"sidebar.html"
                    },
                    'gallery_body': {
                        templateUrl: base_url+"music-gallery.html",
                        controller:"Musicgalleryctrl"
                    }
                }    
            }).state("/home.gallery.movie", {
                url: "/movie",
                views: {
                    'menubar': {
                        templateUrl: base_url+"menubar.html"
                    },
                    'sidebar': {
                        templateUrl: base_url+"sidebar.html"
                    },
                    'gallery_body': {
                        templateUrl: base_url+"movie-gallery.html",
                        controller:"Moivegalleryctrl"
                    }
                }    
            }).state("/login", {
                url: "/login",
                views: {
                    'main_bodys': {
                        templateUrl: base_url+"login.html",
                        controller:"Loginctrl"
                    }
                }    
            });
});
/*first letter capital filter*/
myApp.filter('firstCaps', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});
myApp.directive("filesModel", function($parse) {
    return {
        restrict: 'A',
        link: function($scope, element, attrs) {
            element.on("change", function(event) {
                var files = event.target.files;
                //image preview code by vipul
                    for (var i = 0; i < files.length; i++) {
                         var file = files[i];
                             var reader = new FileReader();
                             reader.onload = $scope.imageIsLoaded; 
                             reader.readAsDataURL(file);
                     }
                //end of code
                $parse(attrs.filesModel).assign($scope, element[0].files);
                $scope.$apply();

            });
        }
    }
}); //end of multiple file upload
//directive end here

//filter for for loop for html
myApp.filter('ranges', function() {
    return function(input, total) {
        total = parseInt(total);

        for (var i = 0; i < total; i++) {
            input.push(i);
        }

        return input;
    };
});
//end of filter here
myApp.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

myApp.directive('ckEditor', function() {
  return {
    require: '?ngModel',
    link: function(scope, elm, attr, ngModel) {
      var ck = CKEDITOR.replace(elm[0]);

      if (!ngModel) return;

      ck.on('pasteState', function() {
        scope.$apply(function() {
          ngModel.$setViewValue(ck.getData());
        });
      });

      ngModel.$render = function(value) {
        ck.setData(ngModel.$viewValue);
      };
    }
  };
});

myApp.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});

//utc filter is remove 'T' and 'Z' from date comming from mysql datetime
myApp.filter('utc', function(){

  return function(val){
    var date = new Date(val);
     return new Date(date.getUTCFullYear(), 
                     date.getUTCMonth(), 
                     date.getUTCDate(),  
                     date.getUTCHours(), 
                     date.getUTCMinutes(), 
                     date.getUTCSeconds());
  };    

});

//textbox accept only letters not a number
myApp.directive('onlyLettersInput', function onlyLettersInput() {
      return {
        require: 'ngModel',
        link: function(scope, element, attr, ngModelCtrl) {
          function fromUser(text) {
            var transformedInput = text.replace(/[^a-zA-Z]/g, '');
            //console.log(transformedInput);
            if (transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
            }
            return transformedInput;
          }
          ngModelCtrl.$parsers.push(fromUser);
        }
      };
    });